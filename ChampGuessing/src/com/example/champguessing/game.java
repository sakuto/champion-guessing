package com.example.champguessing;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class game extends Activity implements OnItemClickListener {
	int click;
	ImageView imageView;
	int rowbases = 0, loops = 0;
	String res = "";
	int score = 0, attempt = 0;
	private Integer[] ar = { R.drawable.one, R.drawable.two, R.drawable.three,
			R.drawable.four, R.drawable.five, R.drawable.six, R.drawable.seven,
			R.drawable.eight, R.drawable.nine };
	private Integer[] ar1 = { R.drawable.two, R.drawable.one, R.drawable.eight,
			R.drawable.three, R.drawable.nine, R.drawable.seven,
			R.drawable.six, R.drawable.five, R.drawable.four };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		rand();
		android.widget.GridView gridview = (android.widget.GridView) findViewById(R.id.gridview1);
		gridview.setVerticalSpacing(2);
		gridview.setHorizontalSpacing(2);
		gridview.setAdapter(new ImageAdapter(this));
		gridview.setOnItemClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public class ImageAdapter extends BaseAdapter {
		private Context context;

		public ImageAdapter(Context c) {
			// TODO Auto-generated method stub
			context = c;
		}

		public int getCount() {
			// TODO Auto-generated method stub
			return ar.length;
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ImageView imageView;
			// imageView
			if (convertView == null) {
				imageView = new ImageView(context);
				imageView.setLayoutParams(new GridView.LayoutParams(110, 110));
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
				imageView.setPadding(2, 2, 2, 2);
			} else {
				imageView = (ImageView) convertView;
			}
			imageView.setImageResource(ar[position]);
			return imageView;
		}
	}

	public void rand() {
		// TODO Auto-generated method stub
		Random random = new Random();
		rowbases = random.nextInt(9);

		if (rowbases == 0) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Aatrox";
			tv.setText(champName);
		} else if (rowbases == 1) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Ahri";
			tv.setText(champName);
		} else if (rowbases == 2) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Akali";
			tv.setText(champName);
		} else if (rowbases == 3) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Alistar";
			tv.setText(champName);
		} else if (rowbases == 4) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Amumu";
			tv.setText(champName);
		} else if (rowbases == 5) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Anivia";
			tv.setText(champName);
		} else if (rowbases == 6) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Annie";
			tv.setText(champName);
		} else if (rowbases == 7) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Ashe";
			tv.setText(champName);
		} else if (rowbases == 8) {
			TextView tv = (TextView) findViewById(R.id.tvName);
			String champName = "Blitzcrank";
			tv.setText(champName);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
		// TODO Auto-generated method stub
		imageView = (ImageView) v;
		imageView.setAdjustViewBounds(true);
		// rand();
		click = position;

		if (click == rowbases) {
			score++;
			rand();
		} else {
			attempt++;
			if (attempt > 2) {
				Intent newActivity = new Intent(game.this, score.class);
				newActivity.putExtra("Score", score);
				startActivity(newActivity);
			}
			rand();
		}

		// rand();
		TextView tv2 = (TextView) findViewById(R.id.tvScore);
		res = String.valueOf(score);
		tv2.setText(res);

	}
}
