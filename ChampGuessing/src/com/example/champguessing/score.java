package com.example.champguessing;




import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class score extends Activity {
	
	int highS=0;
	String c = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_score);
		
		File infile = getBaseContext().getFileStreamPath("highscore.dat");
		if (infile.exists()) {
			try {
				BufferedReader reader = new BufferedReader( new FileReader (infile));
				
				c = reader.readLine().trim();
				highS =Integer.parseInt(c);
				reader.close();
			} catch (Exception e) {
				//Do nothing
				 
			}
		}
		
		final Button StartG = (Button) findViewById(R.id.homeButton);
		// Perform action on click
		StartG.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				// Open Form 2
				Intent newActivity = new Intent(score.this, MainActivity.class);
				startActivity(newActivity);
			}

		});
		
		
		
		TextView tvS = (TextView)findViewById(R.id.Score);
		TextView tvHS = (TextView)findViewById(R.id.HighScore);
		Bundle bundle = getIntent().getExtras();
		int ts = bundle.getInt("Score");
		String b = String.valueOf(ts);
		tvS.setText(b);
		
		if (highS<ts){
			highS=ts;
			
		}
		
		c = String.valueOf(highS);
		tvHS.setText(c);
		
			try {
				FileOutputStream outfile = openFileOutput("highscore.dat", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				p.write(c);
				p.flush(); p.close();
				outfile.close();
			} catch (FileNotFoundException e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			} catch (IOException e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	

}
